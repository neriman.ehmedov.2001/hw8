import java.util.*;

public abstract class Pet {
    // field
    protected String nickname;
    protected HashSet<String> habits = new HashSet<String>();
    protected int age, trickLevel;
    protected Species species;

    // constructors
    public Pet() {
    }

    public Pet(String species, String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        switch(species.toLowerCase()){
            case "dog":
                this.species = Species.Dog;
                break;
            case "domesticCat":
                this.species = Species.DomesticCat;
                break;
            case "roboCat":
                this.species = Species.RoboCat;
                break;
            case "fish":
                this.species = Species.Fish;
                break;
            default:
                this.species = Species.UNKNOWN;
                break;
        }
    }

    // methods
    public void eat() {
        System.out.println("I am eating.");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    // Override methods
    @Override
    public String toString() {
        return species + "{" +
                "nickname = '" + nickname + '\'' +
                ", age = " + age +
                ", trickLevel = " + trickLevel +
                ", habits = " + getHabits() + "}";
    }

    // setters
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setHabits(String habit) {
        this.habits.add(habit);
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setSpecies(String species) {
        switch(species){
            case "dog":
                this.species = Species.Dog;
                break;
            case "domesticCat":
                this.species = Species.DomesticCat;
                break;
            case "robocat":
                this.species = Species.RoboCat;
                break;
            case "fish":
                this.species = Species.Fish;
                break;
            default:
                this.species = Species.UNKNOWN;
                break;
        }
    }

    // getters
    public String getNickname() {
        return nickname;
    }
    public String getHabits() {
        String res = "";
        for(String habit : habits)
            res += (" " + habit);
        return res;
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public Species getSpecies() {
        return species;
    }
}

class Fish extends Pet {
    // constructors
    public Fish() {}
    public Fish(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.Fish;
    }
}
class DomesticCat extends Pet {
    // constructors
    public DomesticCat() {}
    public DomesticCat(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.DomesticCat;
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }
}
class Dog extends Pet {
    // constructors
    public Dog() {}
    public Dog(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.Dog;
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }
}
class RoboCat extends Pet {
    // constructors
    public RoboCat() {}
    public RoboCat(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.RoboCat;
    }
}