import java.util.*;

public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Rahima", "Ahmadov", 2000);
        Man father = new Man("Neriman", "Ahmadov", 2001);
        father.repairCar();
        mother.makeUp();

        Family familyAhamdov = new Family(mother, father);
        mother.setFamily(familyAhamdov);
        father.setFamily(familyAhamdov);

        Woman esma = new Woman("Esma", "Ahmadov", 2020, 160);

        esma.setSchedule("Monday", "go to courses; watch a film");
        esma.setSchedule("Sunday", "do home work");
        esma.setSchedule("Wednesday", "do workout");
        esma.setSchedule("Friday", "read e-mails");
        esma.setSchedule("Saturday", "do shopping");
        esma.setSchedule("Thursday", "visit grandparents");
        esma.setSchedule("Tuesday", "do household");

        familyAhamdov.addChild(esma);
        esma.setFamily(familyAhamdov);

        Dog esmasPet = new Dog("Rex", 5, 49);
        esmasPet.setHabits("eat");
        esmasPet.setHabits("drink");
        esmasPet.setHabits("sleep");
        familyAhamdov.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Man child1 = new Man();
        Woman child2 = new Woman();
        Man child3 = new Man();
        familyAhamdov.addChild(child1);
        familyAhamdov.addChild(child2);
        familyAhamdov.addChild(child3);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(2);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(child3);
        familyAhamdov.countFamily();
    }
}
